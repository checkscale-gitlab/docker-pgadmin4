#!/bin/bash

set -e

#
# Push the Docker image to the GitLab registry as "latest".
#
# Parameters:
#
# $CI_COMMIT_SHA
# $CI_REGISTRY
# $CI_REGISTRY_IMAGE
# $CI_REGISTRY_PASSWORD
# $CI_REGISTRY_USER
#
# @version 0.1.2
# @author David DIDIER
# @see https://gitlab.com/ddidier/docker-tools
#

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $SCRIPT_DIR/commons.sh

print_h1 "Push the Docker image to the GitLab registry as 'latest'"

print_h2 "Script parameters"
echo "CI_COMMIT_SHA = $CI_COMMIT_SHA"
echo "CI_REGISTRY = $CI_REGISTRY"
echo "CI_REGISTRY_IMAGE = $CI_REGISTRY_IMAGE"
echo "CI_REGISTRY_PASSWORD = **********"
echo "CI_REGISTRY_USER = $CI_REGISTRY_USER"

print_h2 "Current Docker informations"
docker info

print_h2 "Authenticating against the GitLab registry"
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

# because we have no guarantee that this job will be picked up by the same runner
# that built the image in the previous step, we pull it again locally
print_h2 "Pulling the built image locally"
docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

print_h2 "Tagging the image as 'latest'"
docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:latest

print_h2 "Pushing the image tagged with 'latest' to the GitLab registry"
docker push $CI_REGISTRY_IMAGE:latest
